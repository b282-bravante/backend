// SECTION

// COments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

/*
there are 2 types of commets"
	1. Single line - //
	2. Multi line - /*<comment>*/


// statements
// statments in programming are instructions that we tell the computer to perform

console.log("Hello World!");


// Whitespace
// Whitespae (basicaly, spaces and line breaks can impact functionality in many computer languages bot NOT in JS)
console.log("Hello World!");

console.

log

(
	"Hello World!"
	)

// Syntax
// In programming, it is the set of rules that describes how statements must be constructed.

// Sample Syntax

// [SECTION] Variables
// It is used to contain data
// Any information that is used by an application is stored in what we call a "memory"

/*
	SYNTAX:
		let/const <variableName>;
*/

let myVariables;
console.log	(myVariables);

/*console.log (hello);
let hello;*/


// Declaring and initializing variables
let productName = "desktop computer";
console.log(productName);

let	productPrice = 18999;
console.log (productPrice);


// data types
// strings - series of character that creates a word, phrase , sentence or anything related to creating text
// Strings in JS can be written using '' or ""

// concatenating strings
// Multiple string values can be combined to create a single string using the "+" symbol

let country = 'philippines';
let province = 'Metro Manila'
let fullAddress = province + ", " + country
console.log(fullAddress)

// escape character (\)
// "\n" refers to creating a new line in between text
let mailAddress = "metro\n\nphilippines"
console.log	(mailAddress);

let message = 'John\'s employees went home early';

// interger
// integer/whole numbers
let headcount = 26;
console.log	(headcount);

// decimal/fractions
let grade =98.7;
console.log(grade);

// exponential notation
let	planetDistance = 2e10;
console.log (planetDistance);

// combining numbers and string

console.log("John's grade last quarter is "+grade)

// boolean values are normally used to store values relating to the state of certain things

let isMarried = false;
let inGoodConduct = true;


// Arrays
// Arrays are special kind of object data type that's used to store multiple values

/*Syntax

	let/const arrayName = [elementA, elementB, elementC, ..]
*/

// similar Data types

let grades = [98.7,92.1,90.2,94.6];
console.log(grades);

// different data types (NOT RECOMMENDED)
let	detail = ["john","smith", 32, true]
console.log(detail);

// Objects is a special kind of data type that's used to mimic real world objects/items

/*SYNTAX:
	let/const objectName = {
		propertyA: value,
		propertyB: value,
	}
	
*/

let person = {
	fullName: "juan dela cruz",
	age: 35,
	isMaried: false,
	contact: ["+63917 123 4567", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}

console.log (person);
console.log (typeof person);
console.log (typeof planetDistance);



