// console.log("hello world")

// [SECTION] if,else if, else statement


	//if Statement
	//SYNTAX
	/* if (conditional statement){ 
			code structure
	}
	*/
	let numA = -1;
	let numB = 1;

	if(numA < 0 ) {
		console.log("Hello");
	} 

	// else if clause
	// executes a statement if previous conditions are false the the specified contditions are true

	if (numA > 0) { //false
		console.log("hello");
	}

	else if (numB > 0) {
		console.log("Hello Again!")
	}

	// else
	// executes if all previous conditions are false

	if (numA > 0) { //false
		console.log("hello");
	}

	else if (numB == 0) {
		console.log("Hello Again!")
	}

	else {
		console.log ("hello Hello Again!")
	}

	// conditional statements used in functions
	/*windSpeed < 30 → Not a typhoon yet.
	windSpeed <= 61 → Tropical depression detected.
	windSpeed >= 62 && windSpeed <= 88 → Tropical storm detected.
	windSpeed >= 89 && windSpeed <= 117 → Severe tropical storm detected*/
	let message;
	function determineTyphoonIntensity (windSpeed) {
		// return statements is used within a function to specify the value that should be returned when the function is called
		if (windSpeed < 30) {
			return "Not a Typhoon Yet";
		}

		else if (windSpeed <= 61){
			return "Tropical Depression Detected";
		}

		else if (windSpeed >= 62 && windSpeed <= 88) {
			return "Tropical Storm Detected";
		}

		else if (windSpeed >= 89 && windSpeed <= 117) {
			return "Severe Tropical Storm Detected";
		}

		else {
			return "Typhoon Detected"
		}
	}
	/*let windSpeed = prompt("Enter Wind Speed")
	message = determineTyphoonIntensity(windSpeed);
	console.log(message);

	if (message == 'Tropical Storm Detected'){
		console.warn(message);
	}*/

// [SECTION] Truthy and Falsy
	// in JS 'truthy' value that is considered true when encountered in a Boolean context
	// values are considered true unless defined otherwise

	/*
	Falsy values/ exception for truthy"
	1. False
	2. 0
	3. -0
	4.""
	5.undefined
	7.NaN (not a number)
	*/

	if (true) {
		console.log("Truthy")
	}

	if (1) {
		console.log("Truthy")
	}

	if ([]) {
		console.log("Truthy")
	}

	// falsy examples\

	if (false) {
		console.log("Falsy")
	}

	if (0) {
		console.log("Falsy")
	}

	if (undefined) {
		console.log("Falsy")
	}

// [SECTION] Conditional (ternary) Operator
	// can be used as an alternative to an "if-else" statement
	// Ternary Operators have an implicit return statement
	/*
	the conditional (ternary) operator takes 3 operands:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy
	*/
	/*
	SYNTAX
		(condition) ? ifTrue : ifFalse;
	*/

	let ternaryResult = (1 < 18) ? true : false;
	console.log("Result of Ternary Operator: "+ternaryResult)

	// Multiple Statement Execution
	let name;

	function isOfLegalAge() {
		name = "john";
		return 'you are of legal age';
	}

	function isUnderAge() {
		name = "Jane";
		return 'you are under the age limit';
	}
	// The parseInt() function converts the input received ito int/number data type
	/*let age = parseInt(prompt("What is your age"));
	console.log(age);

	let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
	console.log("Result of Ternary operation: "+legalAge+", "+name);*/

// [SECTION] Switch Statements
	// can be used as an alternative to an if, else if, and else statement where the data to be used in the condition is of an expected input
	/*SYNTAX:
		switch (expression) {
			case value:
			statement;
			break
		default:
			statement;
			break;
		}
	*/
	// toLowerCase() -  function will change the input received from the prompt into all lowercase letters
	let day = prompt("what day of the week is it today?").toLowerCase();
	console.log(day);

	switch (day) {
		case 'monday':
			console.log("the color of the day is red.");
			break;

		case 'tuesday':
			console.log("the color of the day is orange.");
			break;
		case 'wednesday':

			console.log("the color of the day is yellow.");
			break;

		case 'thursday':			
			console.log("the color of the day is green.");
			break;

		case 'friday':
			console.log("the color of the day is violet.");
			break;

		case 'saturday':
			console.log("the color of the day is indigo.");
			break;

		case 'sunday':
			console.log("the color of the day is blue.");
			break;

		default:
			console.log("please input a valid day");
			break;				
	}



// [SECTION] Try-Catch-Finally Statement
	// "try catch" statements are commonly used for error handling

	function showIntensityAlert(windSpeed){
		try{
			alerat(determineTyphoonIntensity(windSpeed));
		} catch (error) {
			// "typeof" operator is used to check the data typ of the value/expression and returns a string value of what the data type is
			console.log(typeof error);
			// "error.message" - is used to access the information relating to an error object
			console.warn(error.message);
		} finally {
			// continue execution of code regardless of success and failure of code execution in the "try" block to handle/resolve errors
			alert('Intensity updates will show new alert');
		}
	}
	showIntensityAlert(56);