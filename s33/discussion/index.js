// [SECTION] JavaScript Synchronous and Asynchronous
// JS is by default is synchronous, meaning that only one statement us executed at a time
console.log("Hello World!");
console.log("Hello World Again!");

// Aysnchronous means that we can proceed to execute other statements, while consuming is running in the background

// [SECTION] Getting all posts
// The Fetch API allows you to asynchronously request for a resource (data)
// "Promise" is an object that represents the eventual completion or failure of an asynchronous function and its resulting value
/*
SYNTAX:
	fetch('URL')
*/
console.log(fetch('https://jsonplaceholder.typicode.com/posts/'));


// Fetch and .then
/*
SYNTAX:
	fetch('URL')
	.then(response)=> {})
*/

//"fetch" method will return a "promise" that resolves to a "response object"
fetch('https://jsonplaceholder.typicode.com/posts/')
// ".then" methid captures the "response object" and return another promise which will will be eventually be resolved or rejected
.then(response => console.log(response.status));


fetch('https://jsonplaceholder.typicode.com/posts/')
// ".json" method from the response object to convert hthe data retrieved into JSON format to be used in our application
.then((response) => response.json())
// Print the converted JSON value from "fetch request"
.then((json) => console.log(json));


// Async and Await
// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for
// async function fetchData() {

// 	// waits for the "fetch" method to complete then stores the value in the "result" variable
// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts/');

// 	// Result returned by fetch is a returns a promise
// 	console.log(result);

// 	// The returned "Response" is an object
// 	console.log(typeof result);

// 	// We cannot access the content of the "Response" by directly accessing it's body property
// 	console.log(result.body);

// 	// Converts the data from the "Response" object as JSON
// 	let json = await result.json()
// 	// Print out the content of the "Response" object
// 	console.log(json);
// };
// fetchData();


// [SECTION] Creating a post
/*
SYNTAX:
	fetch('URL', options)
	.then((response)=>{})
	.then((response)=>{})
*/

fetch("https://jsonplaceholder.typicode.com/posts/", {
	// Sets the method of the "Request" object to "POST" following REST API
	// Default method is GET
	method: 'POST',
	// Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
	headers: {
		'Content-Type': 'application/json'
	},
	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		title: 'New Post', 
		body: "Hello world!",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Updating a post

// PUT - method used to update the whole document object
// PATCH - method used to update a single/serveral properties

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: "Updated post",
		body: "Hello again!"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});



