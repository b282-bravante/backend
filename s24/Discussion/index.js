// [SECTION] Exponent Operator

	console.warn("Exponent Operator:")
	// Math.pow(base,exponent) - method takes two arguments, the base and the exponent 
	console.log("traditional")
	const firstNum = Math.pow(8,2);
	console.log(firstNum);

	console.log("exponential operator")
	// exponential operator (**) is used for exponentiation
	const secondNum = 8 ** 2;
	console.log (secondNum);

// [SECTION] Template literals
	// Allows to write strings without using concatenation operator "+"
	
	let name = "John";

	let message = "Hello "+name+"! Welcome to programming";
	console.log(message);

	message = `Hello ${name}! Welcome to programming`;
	console.log(message);

	const interestRate = .1;
	const principal = 1000;
	console.log (`The interest on your savings account is: ${principal*interestRate}`);

// [SECTION] Array Destructuring
	// Allows to unpack elements in arrays into distinct variables
	/*
	SYNTAX:
		let/const [variableName, variableName, VariableName ...] = arrayName;
	*/

	const fullName = ['juan', 'dela', 'cruz'];
	console.log(fullName[1]);

	const [firstName, middleName, lastName] = fullName;

	console.log (middleName);

// [SECTION] Object Destructuring
	// Allows to unpack elements in objects into distinct variables
	/*
	let/const [propertyName, propertyName, propertyName ...] = objectName;
	*/

	const person = {
		givenName: "jane",
		maidenName: "dela",
		familyName: "cruz"
	}

	console.log(person.familyName);

	const {givenName, maidenName, familyName} = person;
	console.log(familyName);

// [SECTION] Arrow Functions
	// arrow function allow us to write shorter function syntax
	/*
	SYNTAX:
		let/const variableName = () => {
			statement
		}
	*/

	const students = ["john","jane","judy"];

	students.forEach(function(student){
		console.log(`${student} is a student.`);
	});

	// arrow function
	console.log(`result form using arrow function`)
	students.forEach((student) => {
		console.log(`${student} is a student.`);
	});

// [SECTION] Implicit Return Statement
	// there are instances when you can omit the "return" statement

	// const add = function(x,y){
	// 	return x+y;
	// }

	// const total = add(1,2);
	// console.log(total);

	// arrow function
	console.log(`result form using arrow function`)
	const add =(x,y)=>x+y;
	const total = add(1,2);
	console.log(total);

// [SECTION] Default Function Argument Value
	// provides a default argument value if none is provided when the function is invoked
	const greet = (name = "User")=>{
		return `Goodmorning, ${name}`;
	}
	console.log(greet());
	console.log(greet("b282"));

// [SECTION] Class-Based Object Blueprints
	// allows creation/instantiation of objects using as blueprints

	class Car {
		constructor(brand, name, year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	const myCar = new Car();
	console.log(myCar);

	myCar.brand = 'Ford';
	myCar.name = 'Ranger Raptor';
	myCar.year = 2021;
	console.log(myCar);

	const myNewCar = new Car('toyota', 'Vios', 2021)
	console.log(myNewCar);