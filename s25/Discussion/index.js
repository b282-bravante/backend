/*
	JSON
		-JavaScrip Object Notation
		-is also used in other programming laguages
		-Core JS has a built in JSON object that contains methods for parsing JSON objects and converting strings into JavaScript Objects

		SYNTAX:
		{
			"propertyA": "valueA",
			"propertyB": "valueB",

		};

		JSON are wrapped in curly braces
		Properties and values are wrapped in double quotes
*/

let sample1 = `
	{
		"name": "Mochi",
		"age":"20",
		"address":{
			"city":"Tokyo",
			"country":"Japan"
		}
	}
`;

console.log(sample1);

// create a variable that will hold a "JSON" and create a person object
// log the variable and send ss

let  person = `
	{
		"name":"icoi",
		"age":"20",
		"address": {
			"city":"Silang",
			"country":"Philippines"
		}
	}
`;

console.log(person);
console.log(typeof person);

// JSON.parse() - will return the JSON as an object

console.log(JSON.parse(sample1));
console.log(JSON.parse(person));

// JSON Array
	// JSON Array is an array of  JSON
	let sampleArr = `
		[
			{
				"email": "mochi@gmail.com",
				"password":"mochimochi",
				"isAdmin":false
			},

			{
				"email": "zenitsu@proton.me",
				"password":"agatsuma",
				"isAdmin":true
			},

			{
				"email": "jsonv@gmail.com",
				"password":"friday13",
				"isAdmin":false
			}
		]
	`;

	console.log(sampleArr);
	console.log(typeof sampleArr);

	let parsedSampleArr = JSON.parse(sampleArr);
	console.log(parsedSampleArr);
	console.log(typeof parsedSampleArr);

	console.log(parsedSampleArr.pop());
	console.log(parsedSampleArr);

	// JSON.stringify() - this will stringify JS object as JSON
	// JSON.parse() - does not mutate or update the original JSON


	sampleArr = JSON.stringify(parsedSampleArr);
	console.log(sampleArr);

	// Database (JSON)=> Server/API (JSON to JS object to process) => sent as JSON (frontend/client)

	let jsonArr = `
		[
			"Pizza",
			"hamburger",
			"spaghetti",
			"shanghai",
			"hotdog stick on pinya",
			"pancit bihon"
		]
	`;
	console.log (jsonArr);
	let processedJsonArr = JSON.parse(jsonArr);
	processedJsonArr.pop();
	processedJsonArr.push("bbq");
	jsonArr = JSON.stringify(processedJsonArr);

	console.log (jsonArr);

	// Gather User Details

	/*let firstName = prompt("what is your First name");
	let lastName = prompt("What is your Last name");
	let age = prompt("What is your age?");
	let address = {
		city: prompt("from which city do you live in?"),
		country: prompt("From which country does your city address belong to")
	};

	let otherData = JSON.stringify({
		firstName: firstName,
		lastName: lastName,
		age: age,
		address: address
	});

	console.log(otherData);*/


	let sample3 = `
		{
			"name": "Cardo",
			"age":18,
			"address":{
				"city":"Quiapo",
				"country":"Philippines"
			}
		}
	`
	try{
		console.log(JSON.parse(sample3));
	}
	catch(err){
		console.log("error message");
		console.log(err);
	}
	finally{
		console.log("this will run")
	}