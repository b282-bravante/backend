const express = require ("express");
const mongoose = require ("mongoose");

const app = express();
const port = 3001;


// connecting to mongoDB atlas
mongoose.connect("mongodb+srv://errellebravante:admin123@wdc028-course-booking.hfleqla.mongodb.net/s35",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// connect to mongoDB locally
// allows to handle errors when the initial connection is established
let db = mongoose.connection;


// console.error.bind allows us to print errors in the browser console and in the terminal
db.on("error", console.error.bind(console, "connection to mongoDB is interrupted,"));
db.once("open",()=>console.log("Connected to MongoDB"))



// middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Mongoose Schemas
// Schemas determen the structure of the documents to be written in the database
// Schemas act as blueprints to our data

// the "new" keyword creates a new Schema
// Schema() constructor of the mongoose module to create a new schema object

/*
	SYNTAX:
		const schemaName = new mongoos.Schema({
	
		})	
*/
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}

})

const userSchema = new mongoose.Schema({
	username: String,
	password: String	
});

// [SECTION] models
// models must be in singular form and first letter is capitalized
// first parameter of the mongoose model method indicates the collection in where to store the data
// second parameter is used to specify the schema of the documents that will be store in the mongoDb collection
const Task = mongoose.model("Task",taskSchema);

const Users = mongoose.model ("Users", userSchema);
/*
	Creating a new task
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return a message
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
// [SECTION]
	app.post("/tasks",(req,res) => {
		Task.findOne({
			name: req.body.name
		}).then((result,err) => {
			if (result != null && result.name == req.body.name) {
				return res.send("Duplicate task found!")
			} else {
				let newTask = new Task({
					name: req.body.name
				});
				newTask.save().then((savedTask, saveErr) => {
					return res.status(201).send("New Task created!")
				});
			};
		});
	});

app.post ("/signup",(req,res) => {
	Users.findOne({
		username: req.body.username
	}) // line 93
	.then((result, err) =>{
		if (result != null && result.username == req.body.username) {
			return res.send("Duplicate user found")
		} else if (req.body.username.length === 0 || req.body.password.length === 0){
			return res.send("Username and Password should not be empty")
		} else {
			let newUsers = new Users({
				username: req.body.username
			}); //line 102
			newUsers.save().then((savedUsers,saveErr)=>{
				return res.status(201).send("New User Created")
			}) //line 105
		}//line 101
	})//line 96
})// line 92

/*
	Getting all the tasks
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

// [SECTION]
	app.get("/tasks",(req,res)=>{
		Task.find({}).then((result,err)=>{
			if (err) {
				return console.log(err)
			} else {
				return res.status(200).json({
					data: result
				});
			};
		});
	});


app.listen(port, ()=> console.log(`server running at port ${port}`));