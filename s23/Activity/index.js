// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
    let trainer = {};

// Initialize/add the given object properties and methods

    // Properties
        trainer.name = 'Ash Ketchum';
        trainer.age = 16;
        trainer.friends = {
                hoenn : ['May','Max'],
                kanto : ['Blue','Red'],
            };
        trainer.pokemon = ['Pikachu','Charmander','Squirtle','Bulbasaur'];
    // Methods
        trainer.talk = function(){
                    console.log(this.pokemon[2]+"! I choose you!");
                };
    // Check if all properties and methods were properly added
        console.log(trainer);

// Access object properties using dot notation
    console.log ("using dot notation:");
    console.log(trainer.name);

// Access object properties using square bracket notation
    console.log ("using square bracket notation:");
    console.log(trainer['pokemon']);

// Access the trainer "talk" method
    trainer.talk();

// Create a constructor function called Pokemon for creating a pokemon
    function Pokemon (name, level){

            // properties
            this.name = name;
            this.level = level;
            this.health = (level * 2) + 20;
            this.attack = (level + 20);

            // method
            this.faint = function(){
                if(this.health <= 0){
                console.log(this.name + 'fainted')
                }
            };
            this.tackle = function(target) {
                console.log(this.name+" tackled "+target.name);
                target.health = Number(target.health - this.attack);
                console.log(target.name+" has "+target.health+" health")
                
                if(target.health <= 0){
                target.faint();
                }
            };

            this.faint = function(){            
                console.log(this.name + ' fainted')
            }
    }

// Create/instantiate a new pokemon
    let pikachu = new Pokemon("Pikachu", 20);

// Create/instantiate a new pokemon
    let charizard = new Pokemon("Charizard", 50);

// Create/instantiate a new pokemon
    let blastoise = new Pokemon("Blastoise", 25);

// Invoke the tackle method and target a different object
    pikachu.tackle(charizard);

// Invoke the tackle method and target a different object
    charizard.tackle(blastoise);










//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}