// [SECTION] Objects
	// An object is a data type that is used to represent real-world objects
	// information stored in an object are represented in a "Key : Value" pair

	// Creating objects using initializers/literal notation
	/*
	SYNTAX:
		let objectName = {
			keyA: valueA,
			keyB: valueB,
			...
		}
	*/
	
	let cellphone = {
		name: 'Nokia 3210',
		manufactureDate: 1999
	};

	console.log("Result from creating objects using initializers/literal notation");
	console.log(cellphone);
	console.log(typeof cellphone);


	// Creating objects using constructor function
	/*
	SYNTAX:
		function ObjectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		}
	*/

	function Laptop(name, manufactureDate){
			this.name = name;
			this.manufactureDate = manufactureDate;
		}

	//"new" operator creates an instance of an object
	let laptop = new Laptop("Lenovo", 2008);
	console.log (laptop);

	let myLaptop = new Laptop("Macbook air", 2020);
	console.log (myLaptop);

// [SECTION] Accessing object properties
	// using dot notation
	console.log	(myLaptop.name);
	// using square bracket notation
	console.log	(myLaptop['manufactureDate'])


	// Accessing array objects

	let arrayObects = [laptop,myLaptop];

	// square bracket notation
	console.log(arrayObects[0]['name']);
	// dot notation
	console.log(arrayObects[0].manufactureDate)

// [SECTION] Initializing/Adding, Deleting, Reasigning object properties

	let car = {};

	// initializing/adding objectproperties using dot notation
	car.name = "Honda Civic";
	console.log(car);
	// initializing/adding objectproperties using square bracket notation
	car['manufactureDate'] = 2019
	console.log(car);

	// deleting Object properties
	delete car['manufactureDate'];
	console.log(car);

	// Reasiging object properties
	car.name = "Geely Emgrand";
	console.log(car);

// [SECTION] Object Methods
	// a method is a function which is a property of an object

	let person = {
		name: "John",
		talk: function(){
			console.log("Hello my name is "+ this.name)
		}
	}

	person.talk()

// [SECTION] Real-world Application of Objects
	// Using object literals

	let myPokemon = {
		name: "Pickachu",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log(this.name+" tackled targetPokemon.")
		},
		faint: function(){
			console.log(this.name+" fainted.")
		}
	}

	console.log(myPokemon);
	myPokemon.tackle();
	myPokemon.faint();

	// Using Constructor function
	function Pokemon (name, level){

		// properties
		this.name = name;
		this.level = level;
		this.health = (level * 2) + 20;
		this.attack = (level + 20);

		// method
		this.faint = function(){
			if(this.health <= 0){
			console.log(this.name + 'fainted')
			}
		};
		this.tackle = function(target) {
			console.log(this.name+" tackled "+target.name);
			console.log(target.name+" has "+Number(target.health - this.attack)+" health")
			target.health = Number(target.health - this.attack);
			if(target.health <= 0){
            target.faint();
            }
		};

		this.faint = function(){
			if(this.health <= 0){
			console.log(this.name + ' fainted')
			}
		};
	}


	let pikachu = new Pokemon('Pickachu',10);
	let charmander = new Pokemon('Charmander',15);
	console.log(pikachu);
	console.log(charmander);

	pikachu.tackle(charmander);

	pikachu.tackle(charmander);



