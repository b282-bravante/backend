// console.log ("Hello World!")
// [SECTION] Arithmetic Operators
// Arithmetic Operators allow mathematical operations

let x = 4;
let y = 12;

let sum = x + y;
console.log ("Result of addition operator: " + sum); // result: 16

let difference = x - y;
console.log	("Result of subtraction operator: " + difference); // result -8

let product = x * y;
console.log	("Result of multiplication operator: " + product); // result 48

let quotient = x / y;
console.log	("Result of division operator: " + quotient); // result .333

let remainder =  x % y;
console.log	("Result of remainder operator: " + remainder); // result 4

// [SECTION] Assigment Operator
// Basic Assignment Operator (=)
// The assignment operator assigns the vaule of the right operand to a variable

let	assignmentNumber = 8;
console.log (assignmentNumber); // 8

// Addition Assignment Operator (+=)
// The addition assignment operator adds the value of the right operand and assigns the result to the variable on the left operand

assignmentNumber = assignmentNumber + 2;
console.log	("Result of addition assignment operator: " + assignmentNumber); // result 10

assignmentNumber +=2;
console.log	("Result of addition assignment operator: " + assignmentNumber); // result 12

// Multiple Operators and Parenthesis
/*
M - multiplication 3 * 4 = 12
D - division 12 / 5 = 2.4
A - addition 1 + 2 = 3
S - subtraction 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log	("Result of mdas operation: " + mdas); // result 0.6

/*
P - Parenthesis
E - Exponent
M - multiplication
D - division 
A - addition
S - subtraction

1. 4 / 5 = 0.8
2. 2 - 3 = -1
3. -1 * 0.8 = -0.8
4. 1 + (-0.8) = 0.2 or 0.199999
*/
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log	("Result of mdas operation: " + pemdas); // result 0.6


// Increment and Decrement
// Operators that add or subtract valuues by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

let increment = z++;
console.log ("Result of increment: "+ z) // result 2

let decrement =z--;
console.log ("Result of increment: "+ z) // result 1

// [SECTION] Type Coersion
// Type coercion is the automatic or implicit conversion of valuies from one data type to another
// Values are automatically converted from one data type to another in order to resolve operations

let numA = '10';
let	numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion)

// [SECTION] Comparison Operators
// "Equality Operator (==)"
// checks whether the operands are equal/have the same content
console.log ("Equality Operator (==)")
console.log	(1 == 1); // true
console.log (1 == '1') // true
console.log ("juan" == 'juan') // true
console.log (0 == false) // true

// Strict Equality Operator (===)
// Checks whether the operands are equal/have the same content and also compares the data types of the two values
console.log ("Strict Equality Operator (===)")
console.log	(1 === 1); // true
console.log (1 === '1') // false
console.log ("juan" === 'juan') // true
console.log (0 === false) // false

// Inequality Operator
// Checks whether the oprands are NOT equal/have different content

console.log ("Inequality Operator (!=)")
console.log	(1 != 1); // false
console.log (1 != '1') // false
console.log ("juan" != 'juan') // false
console.log (0 != false) // false

// Strict Inequality Operator (!==)
// Checks whether the operands are NOT equal/have the same content and also compares the data types of the two values
console.log ("Strict Inequality Operator (!==)")
console.log	(1 !== 1); // false
console.log (1 !== '1') // true
console.log ("juan" !== 'juan') // false
console.log (0 !== false) // true

// [SECTION] Relational Operation
let a = 50;
let b = 65;

console.log ("Relational Operators")
// GT or Greater Than operator (>)
let	isGreaterThan = a > b; //false
console.log (isGreaterThan)

// LT or Less Than Operator (<)
let isLessThan = a < b; //true
console.log (isLessThan)

// GTE or Greater than or equal (>=)
let isGTorE = a >= b;
console.log (isGTorE);

// LTE or Less than or equal (>=)
let isLTorE = a <= b;
console.log (isLTorE);

// [SECTION] Logical Operators

let isLegalAge = true;
let isRegistered = false;

// logical AND (&&)
// returns true if both operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of Logical AND Operator: "+allRequirementsMet);

// Logical OR (||)
// returns true if at least one operand is true
let someRequirementsMet  = isLegalAge || isRegistered
console.log("Result of Logical OR Operator: "+someRequirementsMet);

// Logical NOT Operator
// returns opposite value

someRequirementsMet	= !isRegistered	
console.log("Result of Logical NOT Operator: "+someRequirementsMet	)