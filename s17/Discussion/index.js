// [SECTION] Functions
// Functions in JS are lines/block of codes that tell our device/application to perform a certain task when called/invoke

// Function Declarations
/*
	SYNTAX:
		function functionName() {
			code block (statement)
		}
*/

// function - keyword used to define a JS function
// printName - example function name
// Functions are named to be able to be used later in the code.
	function printName() {
		// function block ( {} ) - the statements which comprise the body of the function
		console.log ("My name is John");
	}

// function invocation
	printName();

// [SECTION] Function Declarations vs Expressions
// Function Declarations
// A function can be created through function declaration by using FUNCTION keyword and adding function name
	function  declaredFunction() {
		console.log ("Hello World from declaredFunction()!");
	}

	declaredFunction();

// Function Expressions
	// A function can also be stored in a variable
	// a function expression is an anonymous function assigned to a variableFunction
	// Anonymous function = function without a name
	// function expressions must be declared before invoking.

	let variableFunction = function() {
		console.log ("Hello Again from variableFunction()!");
	}

	variableFunction()

	// function expressions are always invoked/call using variable name
	let funcExpression = function funcName() {
		console.log	("Hello from the other side!");
	}

	//funcName(); //Uncaught ReferenceError: funcName is not defined
	funcExpression();

	// you can reassign declared functions and function expressions to new anonymous function

	declaredFunction = function(){
		console.log("updated declaredFunction!");
	}

	declaredFunction();

	funcExpression = function () {
		console.log("updated funcExpression");
	}

	funcExpression();

	const constantFunc = function() {
		console.log	("Initialized with const!")
	}

	constantFunc();

	// Uncaught TypeError: Assignment to constant variable.
	/*constantFunc = function() {
		console.log ("reinitialized")
	}

	constantFunc();
	*/

// [SECTION] Function Scoping
// Scope is the accessibility/visibility of variables
/*
	JS has 3 types of scope
	1. local/block scope
	2. global scope
	3. function scope
*/

	{
		let localVar = "Armando Perez";
	}

	let globalVar = "Mr. Worldwide";

	// console.log (localVar);
	console.log (globalVar);


	function showNames() {

		// function scoped variables
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet	= "Jane";

	}

	showNames();
	// console.log (functionVar); will result in error
	// console.log (functionConst); will result in error
	// console.log (functionLet); will result in error

	// Nested Functions
	// you can create another function inside a function

	function myNewFunction() {
		let name = "b282";

		function nestedFunction() {
			let nestedName = "PT Class";
			console.log (name)
		}
		nestedFunction();
	}
	myNewFunction();

// [SECTION] Using Alert()
	// alert() alows us to show a small window at the top of our browser page to show information to our users.

	/*alert ("Hello World") // this will run immediately when he page loads

	function showSampleAlert() {
		alert ("hello, user");
	}
	showSampleAlert();*/

// [SECTION] Using Prompt()
	// prompt() allows us to show a small window at the top of the browser to gather user input

	/*let samplePrompt = prompt("Enter your name");
	console.log("Hello, "+samplePrompt)\*/

	function printWelcomeMessage(){
		let firstName = prompt("Enter your First Name");
		let lastName = prompt("Enter your Last Name");
		console.log ("Hello "+ firstName +" "+ lastName);
		console.log ("Welcome to my page!");
	}

	printWelcomeMessage();


// [SECTION] Function Naming Conventions
	// function names should be definitive of the task it will perform
	// it usually contains a verb

	function getCourses() {
		let courses = ["Science 101", "Math 101", "English 101"]
		console.log(courses);
	}
	getCourses();

	// Name your function in small caps
	// follow camelCase when naming variables
	function displayCarinfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}
	displayCarinfo();

	// avoid generic names to avoid confusion within your code
	function get(){
		let name = "Jamie";
		console.log("name");
	}
	get();

	// avoid pointless function names
	function foo(){
		let foo = "Stop using useless function names";
		console.log(foo);
	}
	foo();