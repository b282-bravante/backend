// defines WHEN particular controllers will be used
// contains all the endpoints 

const express = require("express");

// creates a router instance that functions as a middleware and routing system
const router = express.Router();

const taskController = require ("../controllers/taskControllers");


// [SECTION] Routes

// GET
router.get("/", (req,res) => {
	taskController.getAllTasks().then((resultFromController) => res.send(resultFromController) );
});

// POST create a new task
router.post("/",(req,res) => {
	taskController.createTask(req.body).then((resultFromController) => res.send(resultFromController) );
});

// Delete Task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

router.delete("/:id", (req,res)=>{
	taskController.deleteTask(req.params.id).then((resultFromController) => res.send(resultFromController) );
});

// updating a task
router.put("/:id/complete",(req,res) => {
	taskController.updateTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController) );
});

// Getting specific Task 
router.get("/:id",(req,res) => {
	taskController.getOneTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController) );
});

module.exports = router;