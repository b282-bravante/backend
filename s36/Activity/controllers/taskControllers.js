// contains Instructions on how your API will perform its intended tasks
// All operations it can do will be placed in this file

const Task = require("../models/task")

module.exports.getAllTasks = () => {
	return Task.find({})
	.then( result => {
		return result
	});
};

module.exports.createTask = (requestBody) =>{
	let newTask = new Task({
		name: requestBody.name
	});
	return newTask.save().then((task,error) => {
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		};
	})
};

module.exports.deleteTask= (taskId)=>{
	return Task.findByIdAndRemove(taskId).then((removeTask, err) => {
		if (err){
			console.log(err);
			return false
		} else {
			return "Deleted task"
		}
	})
}

module.exports.updateTask = (taskId, newContent) =>{
	return Task.findById(taskId).then((result, err) => {
		if (err){
			console.log(err);
			return false;
		} 

		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return "Task updated."
			}
		})
	})
}

module.exports.getOneTask= (taskId) =>{
	return Task.findById(taskId).then( result => {
		return result
	});
}