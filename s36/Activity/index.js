const express = require("express");
const mongoose = require("mongoose");
//this allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute")

const app = express();

const port = 3001;

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://errellebravante:admin123@wdc028-course-booking.hfleqla.mongodb.net/s36",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// add tasks route
app.use("/tasks", taskRoute);







app.listen(port, () => console.log(`Server running at port ${port}!`));