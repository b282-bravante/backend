// create a server

// "require" directive used to load node.js modules
// "http" module that lets the node.js transfer data using the Hyper Text Transfer Protocol (HTTP)

let http = require("http");

let port=4000;

// "createServer()" - method to create an HTTP server that listens to requests on a specific port and give responses to requests
// "req" -  messages sent by the CLIENT (usually a web browser)
// "res" - messages sent by the SERVER as an answer
const server = http.createServer((req, res) => {
 //
    // writeHead() method calls for the status code and sets the content-type of the response.
    // 200 - means successful request
    //res.writeHead(200, {"Content-Type": "text/plain"});
    //res.end("Hello World\n");
    // req.url - property in node.js that provide the information about the url of the incomming HTTP request
    if (req.url === '/greeting'){
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("Hello World\n");
    } else if (req.url === '/homepage') {
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("this is the homepage");
    } else{
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.end("page not available\n");
    }
});

server.listen(port);

console.log(`Server running on port ${port}`);