let http = require("http");

// mock db

let directory = [
		{
			"name":"errelle",
			"email":"1@email.com"
		},
		{
			"name":"jobert",
			"email":"2@email.com"
		},
	]


http.createServer((request, response) => {

	// get method
	if(request.url === "/users" && request.method === "GET") {
		// Set response output to JSON data type
		response.writeHead(200, {'Content-Type' : 'application/json'});
		// write() is a method in node.js that is used to wrte data the response body in HTTP server
		response.write(JSON.stringify(directory));
		response.end();
	}
	
	// 
	if(request.url == "/users" && request.method == "POST"){
	let requestBody = '';
		request.on('data', function(data){
			requestBody += data;
		});
	 
		request.on('end', function(){
	    requestBody = JSON.parse(requestBody);
	    
	    let newUser = {
	    	"name" : requestBody.name,
	    	"email" : requestBody.email
	    }

	    directory.push(newUser)
	    console.log(directory);

		response.writeHead(200,{'Content-Type': 'application/json'});
		response.write(JSON.stringify(newUser));
		response.end();
		});
	}

}).listen(3000);

console.log('Server is running at localhost: 3000');
