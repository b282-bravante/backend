//console.log("hello world")

/*// Functions
function printInput() {
	let nickName = prompt("Enter your nickname:")
	console.log("Hi, " + nickName);
}

printInput();
*/

// Parameters and Arguments

// (name) - is called a parameter
// Acts as a named variable/container that exists only inside a function
function printName(name){
	console.log("My name is "+name);
}
// ("Juana") - the information/data provided directly into the function is called an argument
printName("Juana");

// variables can also be passed as an argument
let sampleVariable = "koytiks"
printName(sampleVariable);


function checkDivisibilityby8 (num) {

	let remainder = num % 8;

	if (remainder === 0){
		console.log (num+" is divisible by 8")
	}
	else {
		console.log (num+" is not divisible by 8")
	}
}

checkDivisibilityby8(8);
checkDivisibilityby8(30);

// Functions as arguments
function argumentFunction() {
	console.log("this function was passed as an argument before the message was printed")
}

function invokeFunction(argumentFunction){
	argumentFunction();
}

invokeFunction(argumentFunction);

// Multiple "Arguments" will correspond to the number of "parameters" declared in a function in succeeding order

function createFullName (firstName, middleName, lastName){

	console.log(firstName+' '+middleName+' '+lastName);
}

createFullName('Juan', 'Dela', 'Cruz');
