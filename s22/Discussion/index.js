// Array Methods
// JS has Built-in functions and methods for arrays
// this allows us to manipulate and access array items
console.warn ("\n\nMUTATOR METHODS\n\n")
// [SECTION] Mutator Methods
	// Mutator methods are functions that mutate or change an array after they are created

	let fruits = ['Apple','Orange','Kiwi', 'Dragon Fruit'];
	console.log("Original Array:")
	console.log(fruits); //['Apple','Orange','Kiwi', 'Dragon Fruit'];

	// push()
	// Adds an element in the end of an array and returns the updated array's length
	/*
	SYNTAX:
		arrayName.push();
	*/

	let fruitsLength = fruits.push('Rambutan');

	// pop()
	// Removes the last element and return the removed element
	/*
	SYNTAX:
		arrayName.pop();
	*/

	console.log("Current Array");
	console.log(fruits);

	let removedFruit = fruits.pop();

	console.log("Removed Fruit: ");
	console.log(removedFruit);
	console.log("Mutated array from pop() method:");
	console.log(fruits); // ['Apple','Orange','Kiwi', 'Dragon Fruit'];

	// unshift()
	// adds one or more elements at the BEGINNING of an array and it returns the updated array's lenght
	/*
	SYNTAX:
		arrayName.unshift("elemementA");
		arrayName.unshift("elemementA","elementB",...,"elementZ");
	*/

	fruitsLength = fruits.unshift("Aratilis", "Pinya");
	console.log(fruitsLength);
	console.log("Mutated array from unshift() method:");
	console.log(fruits) //["Aratilis", "Pinya", 'Apple','Orange','Kiwi', 'Dragon Fruit'];

	// shift()
	// removes an element at the BEGINNING of an array and reterns the removed element
	/*
	SYNTAX:
		arrayName.shift();
	*/

	console.log("Current Array");
	console.log(fruits);
	removedFruit = fruits.shift();

	console.log("Removed Fruit: ");
	console.log(removedFruit);
	console.log("Mutated array from shift() method:");
	console.log(fruits); // ['Pinya', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit']

	// splice()
	// simultaneously removes an elements from a specifed index and adds element
	/*
	SYNTAX:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
	*/

	console.log("Current Array");
	console.log(fruits); //['Pinya', 'Apple', 'Orange', 'Kiwi', 'Dragon Fruit']

	let splicedFruit = fruits.splice(1,2,"Bayabas","Manga","Saging");
	console.log("Mutated array from splice() method:");
	console.log(fruits); //['Pinya', 'Bayabas', 'Kiwi', 'Dragon Fruit']

	// sort()
	// rearranges the array element in alphanumeric order
	/*
	SYNTAX:
		arrayName.sort();
	*/
	console.log("Current Array");
	console.log(fruits);

	fruits.sort();
	console.log("Mutated array from sort() method:");
	console.log(fruits);


	// reverse()
	// reverses the order of the array elements
	/*
	SYNTAX:
		arrayName.reverse();
	*/
	console.log("Current Array");
	console.log(fruits);

	fruits.reverse();

	console.log("Mutated array from reverse() method:");
	console.log(fruits);

console.warn ("\n\nNON-MUTATOR METHODS\n\n")
// [SECTION] Non-Mutator Methods
	// Non-Mutator methods are functions that do not modify or change an array after they are created

	let countries = ["US", "PH", "CA", "SG", 'TH', 'PH', 'FR', 'DE']

	console.log(countries);

	// indexOf()
	// returns the index number f the first matching element found in an array
	/*
	SYNTAX:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, startingIndex);
	*/

	let firstIndex = countries.indexOf("PH");
	console.log("Result of firstIndex:");
	console.log(firstIndex); // 1

	// lastIndexOf()
	// returns the index number of the last matchin element found in the array

	/*
	SYNTAX:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, startingIndex);
	*/

	let lastIndex = countries.lastIndexOf('PH')
	console.log("Result of lastIndex:");
	console.log(lastIndex); // 5

	// slice()
	// portions/slices elements from an array and return a new array
	/*
	SYNTAX:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex,endingIndex);
	*/

	let slicedArrayA = countries.slice(2);

	console.log("Result of slicedArrayA:");
	console.log(slicedArrayA); // ['CA', 'SG', 'TH', 'PH', 'FR', 'DE']

	let slicedArrayB = countries.slice(2,6);

	console.log("Result of slicedArrayB:");
	console.log(slicedArrayB); // ['CA', 'SG', 'TH', 'PH', 'FR', 'DE']

	// toString()
	// return an array as a string separated by commas
	/*
	SYNTAX:
		arrayName.toString()
	*/

	let stringArray = countries.toString();

	console.log("Result of stringArray:");
	console.log(stringArray)
	console.log(typeof stringArray)

console.warn("ITERATION METHODS")

//[SECTION] Iteration Methods
	// loops designed to perform repetitive task
	// loops over all items in an array

	// forEach()
	// similar to a for loop that iterates on each array of elements
	/*
	SYNTAX:
		arrayName.forEach(function(indivElement){
			statement
		})
	*/
	console.log ("Result of forEach(): ");
	countries.forEach(function(country){
		console.log(country);
	});

	// map()
	// iterates on each element and returns new array with different values depending on the result of the functions's operation
	/*
	SYNTAX:
		let/const resultArray = arrayName.map(function(indivElement){
			statement;
		});
	*/

	let numbers = [1,2,3,4,5];
	let numberMap = numbers.map(function(digit){
		return digit * digit;

	});
	console.log("Original Array:");
	console.log(numbers);
	console.log("Result of map():");
	console.log(numberMap);

	// filter()
	// returns new array that contains elements which meets the given condition
	/*
	SYNTAX:
		let/const resultArray = arrayName.filter(function(indivElement){
			statement;
		});
	*/

	let filterValid = numbers.filter(function(number){
			return number < 3;
	})

	console.log("Original Array:");
	console.log(numbers);
	console.log("Result of filter():");
	console.log(filterValid);

	// includes()
	// checks if the argument passed can be found in the array
	/*
	SYNTAX:
		arrayName.includes(<argumentToFind>)
	*/

	let product = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

	let productFound1 = product.includes('Mouse');
	console.log("Result of includes():")
	console.log(productFound1);

	let productFound2 = product.includes('Headset');
	console.log("Result of includes():")
	console.log(productFound2);
