// [SECTION] While loop
	/*
	SYNTAX:
		while(expression/condition){
			statement
		}
	*/
	
	let count = 5;
	// while the value of the count is not equal to 0
	while (count !== 0) {
		// the current value of count is being printed out
		console.log ("While: "+count);
		// decrease the value by of count by 1 after every iteration to STOP the loop when it reaches 0
		count--;
	}

// [SECTION] Do While Loop
	// works like a while loop but unlike while loops, do-while loops guarantee that the code will be executed at least once

	/*
	SYNTAX:
		do {
			statement
		} while (expression/condition)
	*/

	/*let number = Number(prompt("Give me a number: "));

	do {
		// the current value of number is printed out
		console.log ("Do While: "+ number);
		// Increase the number by 1  until the conditions cant be met
		number += 1;
		// Providing a number 10 or greater will run the code block once and stop the loop
	} while (number < 10);
*/
// [SECTION] For Loop

	/*A for loop is more flexible than while and do-while loops. It consists of three parts:
	1. The "initialization" value that will track the progression of the loop.
	2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
	3. The "finalExpression" indicates how to advance the loop.
	*/
	/*
	SYNTAX:
		for (initialization ; expression/condition ; finalExpression) {
			statement
		}
	*/

	for (let count1=0;count1 <= 10; count1++){		
			console.log("*")
	}


	// .length property
	// characters in string may be counted using the .lenght property
	let myString = "alex";
	console.log("result: "+myString.length);

	// Accessing elements of a string
	console.log(myString[3])
	console.log(myString[2])
	console.log(myString[1])
	console.log(myString[0])

	// loop that will print out individual letters of myString variable

	for(let i=0 ; i < myString.length; i++){
		console.log(myString[i]);
	}

	// Loop that will print out the letters of the name individualy and print out 3 instead when the letter to be printed out is a vowel

	let myName = 'AlEx';
	for(let i=0 ; i < myName.length; i++){
		if (
			myName[i].toLowerCase() == 'a' ||
			myName[i].toLowerCase() == 'e' ||
			myName[i].toLowerCase() == 'i' ||
			myName[i].toLowerCase() == 'o' ||
			myName[i].toLowerCase() == 'u' 
			) { 
		console.log(3);
		} 
		else {
			console.log (myName[i]);

		}
	}

// [SECTION] Continue and Break Statements
	// The "continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
	// The "break" statement is used to terminate the current loop once a match has been found

	for (let count = 0; count <=20; count++) {
		if(count%2 === 0) {
			// tells the code to continue to the next iteration of the loop
			// this ignores all statements located after the continue statement
			continue
		};

		console.log ("Continue and Break: " + count);
		if (count>10) {
			// Tells the code to terminate/stope loop even if the expression/condition of the loop defines that should execute so long as the value of count is that or equal to 20
		// number values after 10 will no longer printed
			break;
		}
		
	}

	// loop that will iterate based on the lenght of the string
	// if the vowel is equal to a continue to the next iteration of the loop
	// if the current letter is equal to r, stop the loop'

	let name = "alexandro";
	for (let i = 0; i < name.length; i++){
		/// If the vowel is equal to a, continue to the next iteration of the loop
		if (name[i].toLowerCase() === "a") {
			console.log("continue to next iteration");
			continue;
		}
		// The current letter is printed out based on its index
		console.log (name[i]);
		// If the current letter is equal to "r", stop the loop
		if (name[i].toLowerCase() === "r") {
			console.log("Break")
			break;
		}
	}