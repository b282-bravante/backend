// CRUD Operations

// Create Operation
// insertOne() - Inserts one document to the collection
db.users.insertOne({
	"firstName": "John",
	"lastName": "Smith"
});

// insertMany() - Inserts multiple documents to the collection
db.users.insertMany([
	{ "firstName": "John", "lastName": "smith" },
	{ "firstName": "Jane", "lastName": "smith" }
]);

// Read Operation
// find() - get all the inserted users
db.users.find();

// Retrieving specific documents
db.users.find({ "lastName": "Doe" });

// Update Operation
// updateOne() - modify one document
db.users.updateOne(
	{
		"_id": ObjectId("648b015809d731544bfec266")
	},
	{
		$set: {
			"email": "johnsmith@gmail.com"
		}
	}
);

// updateMany() - modify multiple documents
db.users.updateMany(
	{
		"lastName":"smith"
	},
	{
		$set: {
			"isAdmin": false
		}
	}

);

// delete operation