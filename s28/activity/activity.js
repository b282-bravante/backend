// S28 - MongoDB CRUD Operations Activity Template:

/*

    Sample solution:

    async function addOneQuery(db) {
        await (

            //add query here

            db.collectionName.insertOne({
                field1: "value1",
                field2: "value2"
            })
        
        );
        
        return(db);
    }

Note: 
    - Do note change the functionName or modify the exports


*/

// 1. Insert a single room (insertOne method) in the rooms collection:

async function addOneFunc(db) {

    await (

        db.users.insertOne({
            "name":"Single",
            "accomodates":2,
            "price":1000,
            "description":"A simple room with all the basic necessities",
            "rooms_available": 10
            "isAvailable": false,

        })
    );


   return(db);

};


// 2. Insert multiple rooms (insertMany method)  in the rooms collection

async function addManyFunc(db) {

    await (

        db.users.insertMany([
        {
            "name":"Double",
            "accomodates":3,
            "price":2000,
            "description":"A room fit for small family going on a vacation",
            "rooms_available": 5
            "isAvailable": false,
        },
        {
            "name":"Queen",
            "accomodates":4,
            "price":4000,
            "description":"A room with a Queen-sized bed perfect for a simple getaway",
            "rooms_available": 15
            "isAvailable": false,
        },

        ])

    );

   return(db);

};

// 3. Use the findOne method to search for a room with the name double.
async function findRoom(db) {
    return await (

        db.users.find(
            {"name":"double";}
            );

    );
};

// 4. Use the updateOne method to update the queen room and set the available rooms to 0.

function updateOneFunc(db) {

   db.users.updateOne(
    {
        "name": "Queen"
    },
    {
        $set:{
            "rooms_available": 0,
        }
    });

};


// 5. Use the deleteMany method to delete all rooms that have 0 rooms available.
function deleteManyFunc(db) {

   db.users.deleteMany(
    { 
        "rooms_available": 0 
    });
};



try{
    module.exports = {
        addOneFunc,
        addManyFunc,
        updateOneFunc,
        deleteManyFunc,
        findRoom
    };
} catch(err){

};

